$.ajax({

    url: "ajax/ajaxDataTableVentas.php",
    success: function(respuesta) {

        console.log("respuesta", respuesta);
    }


})



$(document).ready(function() {
    $('.tablaVentas').DataTable({
        //configurando caracteristicas del Plugin datatable
        "ajax": "ajax/ajaxDataTableVentas.php",
        "deferRender": true,
        "retrive": true,
        "processing": true,

        "language": {
            "decimal": "",
            "emptyTable": "No data available in table",
            "info": "Mostrando _START_ de _END_ hasta _TOTAL_ entradas",
            "infoEmpty": "Showing 0 to 0 of 0 entries",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "No se encontraron resultados",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            }
        }



    });





});


//al hacer click en el boron agregar producto de la tabla
$(".tablaVentas").on("click", ".ProductoAgregar", function() {
    var ProductoId = $(this).attr("idProducto"); //trabaja sobre ajaxDataTableVentas botonera
    console.log("ProductoId", ProductoId);


    //desactivar boton agrgar producto cambio color del boton evita agregar el mismo producto

    $(this).removeClass("btn-primary ProductoAgregar");
    $(this).addClass("btn-default");

    var data = new FormData();
    data.append("productoId", ProductoId); //mandando id producto archivo ajax

    $.ajax({
        url: "ajax/ajaxProductos.php",
        method: "POST",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(resp) {

            var descripcion = resp["descripcion"];
            var stock = resp["stock"];
            var precio = resp["precio_venta"];
            //no permitir agregar producto a la venta si stock esta en cero
            if (stock == 0) {
                swal("No hay stock", "No hay stock disponible del producto seleccionado", "error");
                $("button[idProducto='" + ProductoId + "']").removeClass("btn-default");
                $("button[idProducto='" + ProductoId + "']").addClass("btn-primary ProductoAgregar");
                return;

            }

            //inserta html  con los datos traidos en la vista
            $(".ProductoNuevo").append('<div class="form-row">' +
                '<div class = "form-group col-md-6">' +
                '<div class = "input-group-prepend">' +
                '<span class = "input-group-text" ><button type = "button" class = "btn btn-danger btn-xs  quitarProducto" idProductoB = "' + ProductoId + '"> <i i class = "fa fa-times"></i></button ></span >' +
                '<input type ="text" class = "form-control" name = "agregarProducto" value = "' + descripcion + '" readonly required>' +
                '</div>' +
                '</div>' +
                '<div class = "form-group col-md-3">' +
                '<input type = "number" class = "form-control" id = "nuevoCantidadProducto" name = "nuevoCantidadProducto" min = "1" placeholder = "0"  value = "' + stock + '" required ></div>' +
                '<div class = "form-group col-md-3">' +
                '<div class = "input-group-append ">' +
                '<input type = "number" class = "form-control" id = "nuevoCantidadProducto" name = "nuevoCantidadProducto" min = "1" placeholder = "0" required value = "' + precio + '" >' +
                '<span class = "input-group-text" ><i class = "ion ion-social-usd" ></i><span>' + '</div >' +
                '</div >' +
                '</div>'














            )




        }


    })


});

// al carga la tabla   draw.dt funcion de jquery data table   para hacer una tarea cuando estesmo  usando la tabla
//n cada actualización o uso de la tabla

$(".tablaVentas").on('draw.dt', function() {
    if (localStorage.getItem("eliminarProd") != null) {
        //json.parse convierte Strng a jsgrid-nodata-row
        var listadoIdProd = JSON.parse(localStorage.getItem("eliminarProd"));

        //recorre el json 

        for (var i = 0; i < listadoIdProd.length; i++) {
            //si existe algún elemento con clase reactivarProducto con un atributo idpRODUCTO igual alguno existente en el array cambia color boton agregar producto 
            $(".reactivarBoton[idProducto='" + listadoIdProd[i]["ProductoId"] + "']").removeClass('btn-default');
            $(".reactivarBoton[idProducto='" + listadoIdProd[i]["ProductoId"] + "']").addClass('btn-primary');



        }


    }

})



//BORRAR PROPDUCTO Y VOLVER A HABILITAR EL BOTMN
//al hacer clic sobre cada cruz de producto agregado


var idProductoBorrar = [];
localStorage.removeItem("eliminarProd"); //borrar item que guarda registro d elos botones agregar presionados al recargar la pagina


$(".ventaFormulario").on("click", "button.quitarProducto", function() {
    //elimian a padre que contiene al boton y por lo tanto todo lo demás
    var ProductoId = $(this).attr("idProductoB"); //captura id del producto




    $(this).parent().parent().parent().parent().remove();

    console.log(ProductoId);
    //almacenar en localStorage id del producto a eliminar

    if (localStorage.getItem("eliminarProd") == null) {
        idProductoBorrar = [];

    } else {

        idProductoBorrar.concat(localStorage.getItem("eliminarProd"));



    }

    idProductoBorrar.push({ "ProductoId": ProductoId });
    localStorage.setItem("eliminarProd", JSON.stringify(idProductoBorrar));


    $(".reactivarBoton[idProducto='" + ProductoId + "']").removeClass('btn-default'); //escribe  id del producto como atributo  del botón
    $(".reactivarBoton[idProducto='" + ProductoId + "']").addClass('btn-primary ProductoAgregar'); //pone al botón del color normal







})